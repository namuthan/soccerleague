import { Link } from 'react-router';
import React, { PropTypes } from 'react';
import DocumentTitle from 'react-document-title';

import { LoginLink, NotAuthenticated, Authenticated } from 'react-stormpath';

export default class IndexPage extends React.Component {
  render() {
    return (
      <div className="container">
        <h2 className="text-center">Welcome!</h2>
        <hr />
        <div className="jumbotron">
          <Authenticated>
            <p>You are authenticated!</p>
          </Authenticated>

          <NotAuthenticated>
            <p>You are not authenticated!</p>
          </NotAuthenticated>
        </div>
      </div>
    );
  }
}
